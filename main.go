package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/admin-with-key/factory"
)

func main() {

	reader := bufio.NewReader(os.Stdin)
	log.Println("LifeSecurity - Key Generator")
	fmt.Print("Usuario:")
	user, _ := reader.ReadString('\n')
	user = strings.Replace(user, "\n", "", -1)
	fmt.Println("Llave a cifrar:")
	key, _ := reader.ReadString('\n')
	userType, err := factory.GetUser(user)
	if err != nil {
		log.Fatalf("There was an error creating the user: %s", err)
	}
	//using the methods in slice to select the method to use

	//printing results
	fmt.Printf("Para el usuario %s la encriptacion del texto %s en metodo %s es :%s", userType, key, "method-name", "method-result")
	fmt.Printf("Para el usuario %s la encriptacion del texto %s en metodo %s es :%s", userType, key, "method-name", "method-result")
}
