package methods

import "testing"

func Test_morseImpl_cypherInput(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "sucessful",
			args: args{
				input: "Convert this to Morse",
			},
			want:    "-.-. --- -. ...- . .-. - / - .... .. ... / - --- / -- --- .-. ... .",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mi := &morseImpl{}
			got, err := mi.cypherInput(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("cypherInput() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("cypherInput() got = %v, want %v", got, tt.want)
			}
		})
	}
}
