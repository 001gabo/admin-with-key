package methods

import "testing"

func Test_murcielagoImpl_cypherInput(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "Successful",
			args: args{
				input: "hello world",
			},
			want: "h5669 w926d",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			murimpl := &murcielagoImpl{}
			got, err := murimpl.cypherInput(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("cypherInput() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("cypherInput() got = %v, want %v", got, tt.want)
			}
		})
	}
}
