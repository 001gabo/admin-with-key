package methods

import (
	"github.com/alwindoss/morse"
	"strings"
)

//Converts String to Morse
func (mi *morseImpl) cypherInput(input string) (string, error) {
	h := morse.NewHacker()
	morseCode, err := h.Encode(strings.NewReader(input))
	if err != nil {
		return "", err
	}
	return string(morseCode), nil
}
