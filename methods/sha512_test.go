package methods

import "testing"

func Test_sha512Impl_cypherInput(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "successful",
			args: args{
				input: "hello world",
			},
			want: "309ecc489c12d6eb4cc40f50c902f2b4d0ed77ee511a7c7a9bcd3ca86d4cd86f989dd35bc5ff499670da34255b45b0cfd830e81f605dcf7dc5542e93ae9cd76f",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			shaimpl := &sha512Impl{}
			got, err := shaimpl.cypherInput(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("cypherInput() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("cypherInput() got = %v, want %v", got, tt.want)
			}
		})
	}
}
