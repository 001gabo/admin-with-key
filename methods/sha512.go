package methods

import (
	"crypto/sha512"
	"encoding/hex"
)

//Converts String to SHA512
func (shaimpl *sha512Impl) cypherInput(input string) (string, error) {
	s := sha512.New()
	s.Write([]byte(input))
	result := hex.EncodeToString(s.Sum(nil))
	return result, nil
}
