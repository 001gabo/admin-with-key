package methods

import (
	"crypto/md5"
	"encoding/hex"
)

//converts String to MD5
func (m5 *md5Impl) cypherInput(input string) (string, error) {
	data := []byte(input)
	hash := md5.Sum(data)
	result := hex.EncodeToString(hash[:])
	return result, nil
}
