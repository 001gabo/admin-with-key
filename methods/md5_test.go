package methods

import "testing"

func Test_md5Impl_cypherInput(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "successful",
			args: args{
				input: "hello world",
			},
			want:    "5eb63bbbe01eeed093cb22bb8f5acdc3",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m5 := &md5Impl{}
			got, err := m5.cypherInput(tt.args.input)
			if (err != nil) != tt.wantErr {
				t.Errorf("cypherInput() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("cypherInput() got = %v, want %v", got, tt.want)
			}
		})
	}
}
