package methods

type CypherMethods interface {
	cypherInput(input string) (string, error)
}

type Md5Impl struct{}
type MorseImpl struct{}
type MurcielagoImpl struct{}
type Sha512Impl struct{}
