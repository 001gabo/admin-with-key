package methods

func (murimpl *murcielagoImpl) cypherInput(input string) (string, error) {

	var response string

	murcielagoMap := map[string]string{
		"m": "0",
		"u": "1",
		"r": "2",
		"c": "3",
		"i": "4",
		"e": "5",
		"l": "6",
		"a": "7",
		"g": "8",
		"o": "9",
	}

	for i := 0; i < len(input); i++ {
		char := string(input[i])
		if val, ok := murcielagoMap[char]; ok {
			response = response + val
		} else {
			response = response + char
		}
	}

	return response, nil
}
