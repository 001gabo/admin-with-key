package factory

type adminSecurity1 struct {
	user
}

func newAdminSecurity() iUser {
	return &adminSecurity1{
		user: user{
			Role: "adminSecurity1",
		},
	}
}
