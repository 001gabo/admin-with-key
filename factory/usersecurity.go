package factory

type userSecurity1 struct {
	user
}

func newUserSecurity() iUser {
	return &userSecurity1{
		user: user{
			Role: "userSecurity1",
		},
	}
}
