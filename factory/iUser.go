package factory

type iUser interface {
	withRole(role string)
	getRole() string
}

type user struct {
	Role string
}

func (u *user) withRole(role string) {
	u.Role = role
}

func (u *user) getRole() string {
	return u.Role
}
