package factory

import (
	"errors"
)

func GetUser(userType string) (iUser, error) {
	if userType == "adminSecurity1" {
		return newAdminSecurity(), nil
	} else if userType == "userSecurity1" {
		return newUserSecurity(), nil
	} else {
		return nil, errors.New("the user type is not defined")
	}

}
